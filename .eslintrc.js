module.exports = {
    "extends": ["airbnb", "plugin:react/recommended"],
    "settings": {
      "react": {
        "createClass": "createReactClass",  // Regex for Component Factory to use,
                                            // default to "createReactClass"
        "pragma": "React",  // Pragma to use, default to "React"
        "version": "16.4", // React version, default to the latest React stable release
      },
        "propWrapperFunctions": [ "forbidExtraProps" ]  // The names of any functions used to wrap the
                                                        // propTypes object, e.g. `forbidExtraProps`.
                                                        // If this isn't set, any propTypes wrapped in
                                                        // a function will be skipped.
    },
    "env": {
      "browser": true
    },
    "rules": {
      "react/react-in-jsx-scope": "off",
      "jsx-a11y/anchor-is-valid": [ "error", {
        "components": [ "Link" ],
        "specialLink": [ "hrefLeft", "hrefRight" ],
        "aspects": [ "invalidHref", "preferButton" ]
      }],
      "jsx-quotes": ["error", "prefer-single"],
      "key-spacing": ["error", { "mode": "minimum" }],
      "comma-dangle": ["error", "never"],
      "operator-linebreak": ["error", "after"],
      "no-underscore-dangle": ["error", { "allowAfterThis": true }]
    }
};
