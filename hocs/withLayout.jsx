import { Component } from 'react';
import getComponentDisplayName from '../helpers/getComponentDisplayName';

// This function takes a page and layout...
export default function withLayout(Page, Layout) {
  // ...and returns another component...
  class WithLayout extends Component {
    static getInitialProps(ctx) {
      return Page.getInitialProps ? Page.getInitialProps(ctx) : {};
    }

    render() {
      const { primaryColor, sheetsManager, ...otherProps } = this.props;

      // ... and renders the page inside the layout!
      // Notice that we pass through any additional props
      return (
        <Layout primaryColor={primaryColor} sheetsManager={sheetsManager}>
          <Page {...otherProps} />
        </Layout>
      );
    }
  }
  WithLayout.displayName = `WithLayout(${getComponentDisplayName(Page)})`;
  return WithLayout;
}
