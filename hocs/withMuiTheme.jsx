import { Component } from 'react';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';

import getComponentDisplayName from '../helpers/getComponentDisplayName';

// Wrap Layout into MuiTheme
// Extracts sheetsManager and primaryColor from props.
export default function withMuiTheme(Layout) {
  class WithMuiTheme extends Component {
    // Return the app's primary app color
    static defaultPrimaryColor() {
      // eslint-disable-next-line global-require
      const blue = require('@material-ui/core/colors/blue').default;
      return blue['700'];
    }

    // Generate a MUI theme based on provided primaryColor or default
    // primaryColor
    theme() {
      const {
        primaryColor = this.constructor.defaultPrimaryColor()
      } = this.props;

      const palette = { primary: { main: primaryColor } };
      return createMuiTheme({ palette });
    }

    render() {
      const { sheetsManager, primaryColor, ...otherProps } = this.props;

      // wrap Layout in MuiThemeProvider + CssBaseline
      return (
        <MuiThemeProvider
          theme={this.theme()}
          sheetsManager={sheetsManager}
        >
          <CssBaseline />
          <Layout {...otherProps} />
        </MuiThemeProvider>
      );
    }
  }
  WithMuiTheme.displayName = `WithMuiTheme(${getComponentDisplayName(Layout)})`;
  return WithMuiTheme;
}
