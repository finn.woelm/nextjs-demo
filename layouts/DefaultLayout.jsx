import withMuiTheme from '../hocs/withMuiTheme';
import Header from '../components/Header';

const layoutStyle = {
  margin: 20,
  padding: 20,
  border: '1px solid #DDD'
};

const DefaultLayout = ({ children }) => (
  <div style={layoutStyle}>
    <Header />
    {children}
  </div>
);

export default withMuiTheme(DefaultLayout);
