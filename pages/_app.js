/* eslint-disable react/jsx-filename-extension */

import App, { Container } from 'next/app';
import JssProvider from 'react-jss/lib/JssProvider';

import getPageContext from '../src/getPageContext';

class MyApp extends App {
  constructor(props) {
    super(props);
    this.pageContext = getPageContext();
  }

  componentDidMount() {
    // Remove the server-side injected CSS.
    const jssStyles = document.querySelector('#ssr-material-ui-styles');
    if (jssStyles && jssStyles.parentNode) {
      jssStyles.parentNode.removeChild(jssStyles);
    }

    // Load Roboto font asynchronously to prevent render-blocking CSS
    // eslint-disable-next-line global-require
    require('webfontloader').load({
      google: {
        families: ['Roboto:300,400,500']
      }
    });
  }

  render() {
    const { Component, pageProps } = this.props;
    return (
      <Container>
        {/* Wrap every page in Jss and Theme providers */}
        <JssProvider
          registry={this.pageContext.sheetsRegistry}
          generateClassName={this.pageContext.generateClassName}
        >
          {/* Pass sheetsRegistry to the _document through the renderPage
            * enhancer to render collected styles on server side.
            * Pass sheetsManager to the page, so that the Layout can reference
            * it for createMuiTheme */}
          <Component
            sheetsRegistry={this.pageContext.sheetsRegistry}
            sheetsManager={this.pageContext.sheetsManager}
            {...pageProps}
          />
        </JssProvider>
      </Container>
    );
  }
}

export default MyApp;
