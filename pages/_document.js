/* eslint-disable react/jsx-filename-extension */

import React from 'react';
import Document, { Head, Main, NextScript } from 'next/document';
import { ServerStyleSheet } from 'styled-components';

class MyDocument extends Document {
  static async getInitialProps({ renderPage }) {
    // Render app and page and get the context of the page with collected side effects.
    let sheetsRegistry;
    const sheet = new ServerStyleSheet();
    const page = renderPage(App => (props) => {
      // Material UI: Get page context
      ({ sheetsRegistry } = props);

      // Styled components: Collect styles
      return sheet.collectStyles(<App {...props} />);
    });

    const styleTags = sheet.getStyleElement();

    return {
      ...page,
      styleTags,
      // Styles fragment is rendered after the app and page rendering finish.
      styles: (
        <React.Fragment>
          <style
            id='ssr-material-ui-styles'
            // eslint-disable-next-line react/no-danger
            dangerouslySetInnerHTML={{ __html: sheetsRegistry.toString() }}
          />
        </React.Fragment>
      )
    };
  }

  render() {
    return (
      <html lang='en' dir='ltr'>
        <Head>
          <title>
            My page
          </title>
          <meta charSet='utf-8' />
          {/* Use minimum-scale=1 to enable GPU rasterization */}
          <meta
            name='viewport'
            content={
              'user-scalable=0, initial-scale=1, ' +
              'minimum-scale=1, width=device-width, height=device-height'
            }
          />
          {/* StyledComponents styles */}
          {this.props.styleTags}

          {/* Fallback for browsers without JS support: Load Roboto font
              synchronously */}
          <noscript>
            <link
              rel='stylesheet'
              href='https://fonts.googleapis.com/css?family=Roboto:300,400,500'
            />
          </noscript>
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </html>
    );
  }
}

export default MyDocument;
