import Button from '@material-ui/core/Button';

import withLayout from '../hocs/withLayout';
import DefaultLayout from '../layouts/DefaultLayout';

const AboutPage = () => (
  <div>
    <p>
      This is the about page
    </p>
    <Button variant='raised' color='primary'>
      click me!
    </Button>
  </div>
);

export default withLayout(AboutPage, DefaultLayout);
