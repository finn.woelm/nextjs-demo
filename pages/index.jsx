import { Component } from 'react';
import fetch from 'isomorphic-unfetch';
import styled from 'styled-components';
import Button from '@material-ui/core/Button';
import purple from '@material-ui/core/colors/purple';

import Link from 'next/link';
import withLayout from '../hocs/withLayout';
import DefaultLayout from '../layouts/DefaultLayout';

const ListItem = styled.li`
  list-style: none;
  margin-bottom: 10px;
`;

const H1Upper = styled.h1`
  font-family: 'Arial';
  text-transform: uppercase;
`;

const UlSlim = styled.ul`
  padding: 0;
`;

const Show = ({ name, id }) => (
  <ListItem>
    <Link as={`/p/${id}`} href={`/post?id=${id}`} passHref>
      <Button variant='raised' color='primary'>
        {name}
      </Button>
    </Link>
  </ListItem>
);

class IndexPage extends Component {
  static async getInitialProps() {
    const res = await fetch('https://api.tvmaze.com/search/shows?q=batman');
    const data = await res.json();

    console.log(`Show data fetched. Count: ${data.length}`);

    return { shows: data, primaryColor: purple[500] };
  }

  render() {
    const { shows } = this.props;

    return (
      <div>
        <H1Upper>
          Batman TV Shows
        </H1Upper>
        <UlSlim>
          {shows.map(({ show }) => (<Show key={show.id} name={show.name} id={show.id} />))}
        </UlSlim>
      </div>
    );
  }
}

export default withLayout(IndexPage, DefaultLayout);
