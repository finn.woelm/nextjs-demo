import { Component } from 'react';
import fetch from 'isomorphic-unfetch';

import DefaultLayout from '../layouts/DefaultLayout';

class PostPage extends Component {
  static async getInitialProps({ query }) {
    const { id } = query;
    const res = await fetch(`https://api.tvmaze.com/shows/${id}`);
    const show = await res.json();

    console.log(`Fetched show: ${show.name}`);

    return { show };
  }

  render() {
    const { show } = this.props;

    return (
      <DefaultLayout>
        <h1>
          {show.name}
        </h1>
        <p>
          {show.summary.replace(/<[/]?p>/g, '')}
        </p>
        <img src={show.image.medium} alt={show.name} />
      </DefaultLayout>
    );
  }
}

export default PostPage;
